<?php 
$pageTitle = "Tower Sites";

require_once('templates/header.php');

?>

    <div class="preloader">
        <div class="loader">
            <div class="ytp-spinner">
                <div class="ytp-spinner-container">
                    <div class="ytp-spinner-rotator">
                        <div class="ytp-spinner-left">
                            <div class="ytp-spinner-circle"></div>
                        </div>
                        <div class="ytp-spinner-right">
                            <div class="ytp-spinner-circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--====== PRELOADER PART ENDS ======-->

    <!--====== NAVBAR PART START ======-->

    <section class="header-area">
        <div class="navbar-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg">
                            <a class="navbar-brand" href="#">
                                <img src="assets/img/radata.png" alt="Logo">
                            </a>

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarEight" aria-controls="navbarEight" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                                <span class="toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse sub-menu-bar" id="navbarEight">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item active">
                                        <a class="page-scroll" href="index.php">HOME</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="index.php">ABOUT</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="page-scroll" href="index.php">CONTACT</a>
                                    </li>
                                </ul>
                            </div>
                        </nav> <!-- navbar -->
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </div> <!-- navbar area -->
      </section>

      <section class="map-area">
        <div>
          <h2>Current Tower Sites Available Nation Wide</h2>
        </div>
        <div id="map" style="width:100%; height: 900px"></div>

      </section>
	
     <script>      

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(7.94653, -1.02319),
          zoom: 7
        });
        var infoWindow = new google.maps.InfoWindow;

          // Change this depending on the name of your PHP or XML file
          downloadUrl('realconfig.php', function(data) {
            var xml = data.responseXML;
            var towers = xml.documentElement.getElementsByTagName('tower');
            Array.prototype.forEach.call(towers, function(markerElem) {
              var company = markerElem.getAttribute('company');
              var siteId = markerElem.getAttribute('site-id');
              var district = markerElem.getAttribute('district');
              var location = markerElem.getAttribute('location');
              var status = markerElem.getAttribute('status');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = company
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));
              
              var text = document.createElement('text');
              text.textContent = "Site ID: " + siteId + "District: " + district
              infowincontent.appendChild(text);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = "Location: " + location +  ", Site Status: " + status
              infowincontent.appendChild(text);              

              var marker = new google.maps.Marker({
                map: map,
                position: point,                
              });
              ``
              marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
              });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDr1w9upWQUvEuNCBEgFH_N25noI_JBG9E&callback=initMap"></script>


<?php require_once('templates/footer.php'); ?>